

var express = require('express')
var http = require('http')
var mysql = require('mysql')
var bodyParser = require('body-parser')
var app = express()
var request = require("request");

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))


//Creamos la conexion a la DB pruebaTec
var coneccion = mysql.createConnection({
	host: '127.0.0.1',
	user: 'root',
	password: 'toor',
	database: 'pruebaTec'   
});

app.get('/', (req, res) => {
	res.status(200).send("John Gonzalez ApiRest")
});

app.get('/vehiculo', (req, res) => {
	var sqlGet = "select * from vehiculos";
	coneccion.query(sqlGet,(error, rows, colu) => {
		if(error){
			res.write(JSON.stringify({
				error: true,
				error_object: error
			}));
			res.end();
		}else{
			res.write(JSON.stringify(rows));
			res.end();
		}
	});
});

app.post('/vehiculo', (req, res) => {
	var sqlCreate = "INSERT INTO VEHICULOS (idVehiculo, marca, color) VALUES (NULL, ";
	sqlCreate += "'" + req.marca + "', ";
	sqlCreate += "'" + req.color + "', ";
	sqlCreate += "NOW())";
	coneccion.query(sqlCreate,(error, rows, colu) => {
		if(error){
			res.write(JSON.stringify({
				error: true,
				error_object: error
			}));
			res.end();
		}else{
			var idVehi = rows.insertId;
			res.write(JSON.stringify({
				error: false,
				idVehiculo: isVehi
			}));
			res.end;
		}
	});
});

app.delete('/vehiculo', (req, res) => {
	var sqlDel = "DELETE FROM VEHICULOS WHERE idVehiculo = '" + req.idVehiculo + "'";
	coneccion.query(sqlDel,(error, rows, colu) => {
		if(error){
			res.write(JSON.stringify({
				error: true,
				error_object: error
			}));
			res.end();
		}else{
			res.write(JSON.stringify({
				error: false,
				desc: "Vehiculo eliminado"
			}));
			res.end;
		}
	});
});

app.get("/consumoRestApi", (req, res) => {
	var cargaUrl = "https://jsonplaceholder.typicode.com/todos";
	request({
		url:cargaUrl,
		json:false
	}, (error, respon, body) => {
		if(error){
			res.write(JSON.stringify({
				error: true,
				error_object: error
			}));
			res.end();
		}else{
			res.send(body)
		}
	})
	
})

http.createServer(app).listen(8001, () => {
	console.log('Server started at http://localhost:8001');
});